const initialState = {
    angkaSekarang: 0,
}

function angkaReducer(state = initialState, action){
    switch (action.type) {
        case 'TAMBAH_ANGKA':
            return {...state, angkaSekarang: state.angkaSekarang + 1, data: action.data}
        case 'KURANG_ANGKA':
            return {...state, angkaSekarang: state.angkaSekarang - 1}
        case 'RESET':
            return {...state, angkaSekarang: 0}
        default:
            return state
    }
}

export default angkaReducer