import React from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import {useSelector, useDispatch} from 'react-redux'
import {kuranginAngka, tambahinAngka} from './actions'
const CounterComponent = () => {

    const dispatch = useDispatch()
    const angkaSekarang = useSelector(state => state.angkaReducer.angkaSekarang)
    // console.log(angkaSekarang, "angkaSekarang")
    return(
        <View>
            <Text>Hitung: {angkaSekarang}</Text>
            <View>
                <TouchableOpacity onPress={() => {
                    dispatch(kuranginAngka())
                    // dispatch({type: 'TAMBAH_ANGKA'})
                }}>
                    <Text>Kurang (-)</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    dispatch(tambahinAngka())
                    // dispatch({type: 'TAMBAH_ANGKA'})
                }}>
                    <Text>Tambah (+)</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {
                    // dispatch(tambahinAngka())
                    dispatch({type: 'RESET'})
                }}>
                    <Text>RESET</Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

export default CounterComponent