import { createStore, applyMiddleware, combineReducers } from 'redux'
import angkaReducer from '../reducers'
import reduxLogger from 'redux-logger'

const allReducer = combineReducers({angkaReducer})
const store = createStore(allReducer, applyMiddleware(reduxLogger))

// store.subscribe(() => store.getState())

export default store