/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect, useState} from 'react';
import {Provider } from 'react-redux'
import CounterComponent from './src';
import store from './src/store';


import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TextInput,
  Button,
  Alert
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import axios from 'axios';
const Stack = createNativeStackNavigator()
const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
  const [todoListData, setTodos] = useState([])
  // GANTI COMPONENT DID MOUNT
  useEffect(() => {
    axios.get('https://jsonplaceholder.typicode.com/todos')
      .then(responseData => {
        
        // let todos = responseData.data
        // todos = todos.slice(0, 10)
        // setTodos(todos)
        
      }).catch(err => {
        console.log('ERR',err)
      })
  }, [])
  console.log(todoListData);
  return (
   
    <Provider store={store}>
      <CounterComponent />
    </Provider>
    // <SafeAreaView style={backgroundStyle}>
    //   <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
    //   <ScrollView
    //     contentInsetAdjustmentBehavior="automatic"
    //     style={backgroundStyle}>
    //     <Header />
    //     <View
    //       style={{
    //         backgroundColor: isDarkMode ? Colors.black : Colors.white,
    //       }}>
    //       <Section title="Step One">
    //         Edit <Text style={styles.highlight}>App.js</Text> to change this
    //         screen and then come back to see your edits.
    //       </Section>
    //       <Section title="See Your Changes">
    //         <ReloadInstructions />
    //       </Section>
    //       <Section title="Debug">
    //         <DebugInstructions />
    //       </Section>
    //       <Section title="Learn More">
    //         Read the docs to discover what to do next:
    //       </Section>
    //       <LearnMoreLinks />
    //     </View>
    //   </ScrollView>
    // </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

const HomeScreen = () => {
  const [todoListData, setTodos] = useState([])
  const [textInput, setTextInput] = useState('')
  // GANTI COMPONENT DID MOUNT
  useEffect(() => {
    axios.get('http://code.aldipee.com/api/v1/todos')
      .then(responseData => {
        console.log(responseData.data.results)
        setTodos(responseData.data.results)
      }).catch(err => {
        console.log('ERR',err)
      })
  }, [])
  console.log(todoListData, "<<")

  const onChangeText = (value) => {
    setTextInput(value)
  }
  const onSend = () => {
    const body = {
      userId: 1,
      title: textInput,
      completed: false
    }

    axios.post('https://jsonplaceholder.typicode.com/todos', body).then(resp => {
      console.log(resp.data)
      Alert.alert('Data bertambah')
      axios.get('https://jsonplaceholder.typicode.com/todos')
      .then(responseData => {
        let todos = responseData.data
        todos = todos.slice(0, 10)
        setTodos(todos)
        
      }).catch(err => {
        console.log('ERR',err)
      })
    })
  }

  return (<View>
 
    </View>)
}

export default App;
